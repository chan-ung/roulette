//
//  ViewController.swift
//  LunchWheelApp
//
//  Created by skia01 on 2020/04/29.
//  Copyright © 2020 skia01. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // %%%%%%%%%% predefining parameters
    @IBOutlet var wheel_Image: UIImageView!
    let rot_freq: Double = 20.0 // write in Hz scale
    // (plz set under 26Hz)
    // %%%%%%%%%%

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        let shapeLayer1 = CAShapeLayer()
        let path2 = UIBezierPath()
        path2.move(to: CGPoint(x: 665, y: 546))
        path2.addLine(to: CGPoint(x: 720, y: 526))
        path2.addLine(to: CGPoint(x: 720, y: 566))
        path2.close()

        shapeLayer1.path = path2.cgPath
        shapeLayer1.lineWidth = 10
        shapeLayer1.strokeColor = UIColor.red.cgColor

        view.layer.addSublayer(shapeLayer1)
    }

    @IBAction func rotateButton(_: Any) {
        let rotationAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: .pi * 2.0)
        rotationAnimation.duration = 1 / rot_freq
        rotationAnimation.isCumulative = true
        rotationAnimation.repeatCount = .infinity
        wheel_Image?.layer.add(rotationAnimation, forKey: "rotationAnimation")
    }

    /// pause rotation when 'pause button' click
    func pauseAnimation() {
        let pausedTime = wheel_Image.layer.convertTime(CACurrentMediaTime(), from: nil)
        wheel_Image.layer.speed = 0.0
        wheel_Image.layer.timeOffset = pausedTime
    }

    /// resume paused rotation when 'pause button' click
    func resumeAnimation() {
        let pausedTime = wheel_Image.layer.timeOffset
        wheel_Image.layer.speed = 1.0
        wheel_Image.layer.timeOffset = 0.0
        wheel_Image.layer.beginTime = 0.0
        let timeSincePause = wheel_Image.layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        wheel_Image.layer.beginTime = timeSincePause
    }

    @IBAction func pauseButton(_: UIButton) {
        if wheel_Image.layer.speed == 0.0 {
            resumeAnimation()
        } else { pauseAnimation() }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
